# 如何使用

## 安装所需包

在arm的设备上
```
devel-su
pkcon install zypper
zypper ar -f http://repo.merproject.org/obs/home:/birdzhang:/rime/sailfishos_latest_armv7hl/ birdzhang-rime
zypper ref birdzhang-rime
zypper in rime librime-devel
```
在模拟器或平板上
```
sudo pkcon install zypper
sudo zypper ar -f http://repo.merproject.org/obs/home:/birdzhang:/rime/sailfishos_latest_i486/ birdzhang-rime
sudo zypper ref birdzhang-rime
sudo zypper in rime librime-devel
```

#### 注意

当显示 `File 'repomd.xml' from repository 'Personal birdzhang sailfish repository' is signed with an unknown key '911318D420DB486F'. Continue? [yes/no] (no):`, 的时候输入 **yes**

当显示 `Package is not signed! ... Abort, retry, ignore? [a/r/i] (a): `, 输入 **i**(忽略)



## 调试

从 https://github.com/rime/librime/tree/master/data/minimal 下载data数据, 或者用 `git clone https://github.com/rime/librime.git --depth=1` 将代码clone下来。

在data/minimal目录输入 `rime_deployer --compile luna_pinyin.schema.yaml` 部署, 等待一会。

现在你可以用 `echo nihao | rime_api_console ` 来测试了，它会输出
```
[root@Sailfish bin]# echo nihao|rime_api_console 
initializing...
message: [0] [deploy] start
message: [0] [deploy] success
ready.
schema: luna_pinyin / 朙月拼音
status: composing 
[ni hao]|
page: 1  (of size 5)
1. [你好]
2.  妳好 
3.  逆號 
4.  你 
5.  擬 
```

## 扩展

更多的输入法见 [输入方案](https://github.com/search?q=org%3Arime+%E8%BC%B8%E5%85%A5%E6%96%B9%E6%A1%88&unscoped_q=%E8%BC%B8%E5%85%A5%E6%96%B9%E6%A1%88)

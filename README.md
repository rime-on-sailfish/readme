# How to use

## Install packages

On arm devices
```
devel-su
pkcon install zypper
zypper ar -f http://repo.merproject.org/obs/home:/birdzhang:/rime/sailfishos_latest_armv7hl/ birdzhang-rime
zypper ref birdzhang-rime
zypper in rime librime-devel
```
On Emulator/Jolla tablet
```
sudo pkcon install zypper
sudo zypper ar -f http://repo.merproject.org/obs/home:/birdzhang:/rime/sailfishos_latest_i486/ birdzhang-rime
sudo zypper ref birdzhang-rime
sudo zypper in rime librime-devel
```

#### Notice

When display `File 'repomd.xml' from repository 'Personal birdzhang sailfish repository' is signed with an unknown key '911318D420DB486F'. Continue? [yes/no] (no):`, input **yes**

When display `Package is not signed! ... Abort, retry, ignore? [a/r/i] (a): `, type **i**(ignore)



## Debug

Download datas from https://github.com/rime/librime/tree/master/data/minimal , you can use `git clone https://github.com/rime/librime.git --depth=1` to fetch the repo.

In datas directory, put `rime_deployer --compile luna_pinyin.schema.yaml`, and wait a moment.

Now you can use `echo nihao | rime_api_console ` to test, it will print
```
[root@Sailfish bin]# echo nihao|rime_api_console 
initializing...
message: [0] [deploy] start
message: [0] [deploy] success
ready.
schema: luna_pinyin / 朙月拼音
status: composing 
[ni hao]|
page: 1  (of size 5)
1. [你好]
2.  妳好 
3.  逆號 
4.  你 
5.  擬 
```

## Extend

Find more datas from [输入方案](https://github.com/search?q=org%3Arime+%E8%BC%B8%E5%85%A5%E6%96%B9%E6%A1%88&unscoped_q=%E8%BC%B8%E5%85%A5%E6%96%B9%E6%A1%88)
